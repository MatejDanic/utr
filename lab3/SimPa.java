import java.io.*;
import java.util.*;

class Prijelaz3 {
	String trenutnoStanje, ulazniZnak, znakStoga, novoStanje, nizZnakovaStoga;
	
	public Prijelaz3(String trenutnoStanje, String ulazniZnak, String znakStoga, String novoStanje, String nizZnakovaStoga) {
		this.trenutnoStanje = trenutnoStanje;
		this.ulazniZnak = ulazniZnak;
		this.znakStoga = znakStoga;
		this.novoStanje = novoStanje;
		this.nizZnakovaStoga = nizZnakovaStoga;
	}
	
	public String toString() {
		
		return trenutnoStanje + "," + ulazniZnak + "," + znakStoga + "->" + novoStanje + "," + nizZnakovaStoga;
		
	}
}

public class SimPa {
	
	static List<Prijelaz3> prijelazi = new ArrayList<>();
	static String pocetnoStanje;
	static String stanje;
	static Character pocetniZnak;
	static String stog;
	
	static List<String> skupNizova = new LinkedList<>();
	static Set<String> skupStanja = new TreeSet<>();
	static Set<String> skupSimbola = new TreeSet<>();
	static Set<String> skupZnakova = new TreeSet<>();
	static Set<String> skupPrihvStanja = new TreeSet<>();
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		String[] nizovi, stanja, simboli, prihvStanja, funkcija, ulaz, znakovi, izlaz;
		String s;
		
//		1. red
		s = reader.readLine();		
		nizovi = s.split("\\|");
		for (String niz : nizovi) {
			skupNizova.add(niz);
		}
		
//		2. red
		s = reader.readLine();
		stanja = s.split("\\,");
		for (String stanje : stanja) {
			skupStanja.add(stanje);
		}
		
//		3. red
		s = reader.readLine();	
		simboli = s.split("\\,");
		for (String simbol : simboli) {
			skupSimbola.add(simbol);
		}
		
//		4. red
		s = reader.readLine();
		znakovi = s.split("\\,");
		for (String znak : znakovi) {
			skupZnakova.add(znak);
		}
		
//		5. red
		s = reader.readLine();
		prihvStanja = s.split("\\,");
		for (String prihvStanje : prihvStanja) {
			skupPrihvStanja.add(prihvStanje);
		}
		
//		6. red
		s = reader.readLine();
		pocetnoStanje = s;
		
//		7. red
		s = reader.readLine();
		pocetniZnak = s.charAt(0);
		
//		8. red i ostali redovi
		
		while (true) {
		
			s = reader.readLine();
			if (s == null) break;
			if (s.isEmpty()) break;
			funkcija = s.split("->");
			ulaz = funkcija[0].split("\\,");
			izlaz = funkcija[1].split("\\,");
			prijelazi.add(new Prijelaz3(ulaz[0], ulaz[1], ulaz[2],  izlaz[0], izlaz[1]));
			
		}
		for (String niz : skupNizova) {
			pokreniPA(niz);
		}
	}


	private static void pokreniPA(String niz) {
		
		String[] znakovi = niz.split("\\,");
		stanje = pocetnoStanje;
		stog = "";
		stog += pocetniZnak;
		String znakStoga;
		String izlaz = stanje + "#" + stog;
		
		int i = 0;
		for (String znak : znakovi) {
			i++;
			if (stog.length() != 0 && postojiPrijelaz(stanje, stog.substring(0, 1))) {
				if (postojiPrijelaz$(stanje, stog.substring(0, 1))) {
					izlaz += epsilon();
				} 
				if (stog.length() != 0 && postojiPrijelazZnak(stanje, znak, stog.substring(0, 1))) {
					
					znakStoga = prijelazZnakStoga(stanje, znak, stog.substring(0, 1));
					stanje = prijelazStanje(stanje, znak, stog.substring(0 , 1));
					
					izlaz += "|" + stanje + "#";
					if (znakStoga.equals("$")) {
						stog = stog.substring(1);
						if (stog.length() == 0) {
							izlaz += "$";
							break;
						}
					} else if (znakStoga.length() >= 2) {
						stog = znakStoga + stog.substring(1);
					}
					izlaz += stog;
				} else {
					izlaz += "|fail";
					break;
				}
			} else {
				izlaz += "|fail";
				break;
			}
			
			if (i == znakovi.length && stog.length() != 0) {
				if (skupPrihvStanja.contains(stanje)) break;
				izlaz += epsilon();
			} else if (stog.length() == 0) {
				izlaz += "|fail";
				break;
			}
			
		}
		if (skupPrihvStanja.contains(stanje) && !izlaz.endsWith("fail")) {
			izlaz += "|1";
		} else {
			izlaz += "|0";
		}
		System.out.println(izlaz);
	}

	private static String epsilon() {
		String znakStoga, izlaz = "";
		while (stog.length() != 0 && postojiPrijelaz$(stanje, stog.substring(0, 1))) {
			
			znakStoga = prijelazZnakStoga(stanje, "$", stog.substring(0, 1));
			stanje = prijelazStanje(stanje, "$", stog.substring(0 , 1));
			
			izlaz += "|" + stanje + "#";
			if (znakStoga.equals("$")) {
				stog = stog.substring(1);
				if (stog.length() == 0) {
					izlaz += "$";
					break;
				}
			} else if (znakStoga.length() == 2) {
				stog = znakStoga + stog.substring(1);
			}
			izlaz += stog;
			if (skupPrihvStanja.contains(stanje)) break;
		}
		return izlaz;
	}


	private static String prijelazZnakStoga(String stanje, String znak, String znakStoga) {
		String rezultat = "";
		for (Prijelaz3 prijelaz : prijelazi) {
			if (prijelaz.trenutnoStanje.equals(stanje)
					&& prijelaz.ulazniZnak.equals(znak)
					&& prijelaz.znakStoga.equals(znakStoga)) {
				rezultat = prijelaz.nizZnakovaStoga;
				break;
			}
		}
		return rezultat;
	}


	private static String prijelazStanje(String stanje, String znak, String znakStoga) {
		String rezultat = "";
		for (Prijelaz3 prijelaz : prijelazi) {
			if (prijelaz.trenutnoStanje.equals(stanje)
					&& prijelaz.ulazniZnak.equals(znak)
					&& prijelaz.znakStoga.equals(znakStoga)) {
				rezultat = prijelaz.novoStanje;
				break;
			}
		}
		return rezultat;
	}


	private static boolean postojiPrijelaz(String stanje, String znakStoga) {
		boolean postoji = false;
		for (Prijelaz3 prijelaz : prijelazi) {
			if (prijelaz.trenutnoStanje.equals(stanje)
					&& prijelaz.znakStoga.equals(znakStoga)) {
				postoji = true;
			}
		}
		return postoji;
	}
	
	private static boolean postojiPrijelaz$(String stanje, String znakStoga) {
		boolean postoji = false;
		for (Prijelaz3 prijelaz : prijelazi) {
			if (prijelaz.trenutnoStanje.equals(stanje)
					&& prijelaz.znakStoga.equals(znakStoga)
					&& prijelaz.ulazniZnak.equals("$")) {
				postoji = true;
			}
		}
		return postoji;
	}
	
	private static boolean postojiPrijelazZnak(String stanje, String znak, String znakStoga) {
		boolean postoji = false;
		for (Prijelaz3 prijelaz : prijelazi) {
			if (prijelaz.trenutnoStanje.equals(stanje)
					&& prijelaz.znakStoga.equals(znakStoga)
					&& prijelaz.ulazniZnak.equals(znak)) {
				postoji = true;
			}
		}
		return postoji;
	}
}