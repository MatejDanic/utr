import java.io.*;
import java.util.*;

class Prijelaz5 {
	String trenutnoStanje;
	char znakNaTraci;
	String novoStanje;
	char noviZnakNaTraci;
	char pomakGlave;
	
	
	public Prijelaz5(String trenutnoStanje, char znakNaTraci, String novoStanje, char noviZnakNaTraci, char pomakGlave) {
		this.trenutnoStanje = trenutnoStanje;
		this.znakNaTraci = znakNaTraci;
		this.novoStanje = novoStanje;
		this.noviZnakNaTraci = noviZnakNaTraci;
		this.pomakGlave = pomakGlave;
	}
	
	public String toString() {
		
		return trenutnoStanje + "," + znakNaTraci + "->" + novoStanje + "," + noviZnakNaTraci + "," + pomakGlave;
		
	}
}

public class SimTS {
	
	
	static Set<String> skupStanja = new TreeSet<>();
	static Set<String> skupUlaznihZnakova = new TreeSet<>();
	static Set<String> skupZnakovaTrake = new TreeSet<>();
	static Set<String> skupPrihvStanja = new TreeSet<>();
	static List<Prijelaz5> prijelazi = new LinkedList<>();
	static String znakPrazneCelije, zapisTrake, pocetnoStanje;
	static int pocetniPolozaj;
	
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		String[] stanja, znakovi, znakoviTrake, prihvStanja;
		String s;
		
//		1. red
		s = reader.readLine();		
		stanja = s.split("\\,");
		for (String stanje : stanja) {
			skupStanja.add(stanje);
		}
		
//		2. red
		s = reader.readLine();
		znakovi = s.split("\\,");
		for (String znak : znakovi) {
			skupUlaznihZnakova.add(znak);
		}
		
//		3. red
		s = reader.readLine();	
		znakoviTrake = s.split("\\,");
		for (String znakTrake : znakoviTrake) {
			skupZnakovaTrake.add(znakTrake);
		}
		
//		4. red
		s = reader.readLine();
		znakPrazneCelije = s;
		
		
//		5. red
		s = reader.readLine();
		zapisTrake = s;
		
//		6. red
		s = reader.readLine();
		prihvStanja = s.split("\\,");
		for (String prihvStanje : prihvStanja) {
			skupPrihvStanja.add(prihvStanje);
		}
		
//		7. red
		s = reader.readLine();
		pocetnoStanje = s;
		
//		8. red
		s = reader.readLine();
		pocetniPolozaj = Integer.parseInt(s);
		
		
//		9. red i ostali redovi
		
		while (true) {
		
			s = reader.readLine();
			if (s == null) break;
			if (s.isEmpty()) break;
			String[] funkcija = s.split("->");
			prijelazi.add(new Prijelaz5(					
					funkcija[0].split("\\,")[0], // trenutnoStanje
					funkcija[0].split("\\,")[1].charAt(0), // znakNaTraci
					funkcija[1].split("\\,")[0], // novoStanje
					funkcija[1].split("\\,")[1].charAt(0), // noviZnakNaTraci
					funkcija[1].split("\\,")[2].charAt(0) // pomakGlave
					));
		}
		System.out.println("\n" + pokreniTS(zapisTrake) + "\n");
	}


	private static String pokreniTS(String traka) {
		String trenutnoStanje = pocetnoStanje;
		int trenutniPolozaj = pocetniPolozaj;
		boolean prijelazPostoji = false;
		
		while (true) {
			prijelazPostoji = false;
			for (Prijelaz5 prijelaz : prijelazi) {
				
				if (prijelaz.trenutnoStanje.equals(trenutnoStanje) && prijelaz.znakNaTraci == traka.charAt(trenutniPolozaj)) {
					
					trenutnoStanje = prijelaz.novoStanje;
					traka = traka.substring(0, trenutniPolozaj) + prijelaz.noviZnakNaTraci + traka.substring(trenutniPolozaj+1);
//					System.out.println(traka);
					
					if (prijelaz.pomakGlave == 'R' && trenutniPolozaj < traka.length()-1) {
						trenutniPolozaj++;
					} else if (prijelaz.pomakGlave == 'L' && trenutniPolozaj > 0){
						trenutniPolozaj--;
					} else {
						break;
					}
					
					prijelazPostoji = true;
					break;
				}
			}
			if (!prijelazPostoji) {
				break;
			}
		}
		
		return (trenutnoStanje + "|" + trenutniPolozaj + "|" + traka + "|" + (skupPrihvStanja.contains(trenutnoStanje) ? "1" : "0"));
	}		
}