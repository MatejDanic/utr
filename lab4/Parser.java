import java.io.*;

//		Gramatika :
//
//		S -> aAB | bBA
//		A -> bC | a
//		B -> ccSbc | $
//		C -> AA



public class Parser {
	
	static String ulazniNiz;
	static String izlaz;
	static int duljina;
	static int i;
	
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		ulazniNiz = reader.readLine();
		duljina = ulazniNiz.length();
		i = 0;
		System.out.println();
		System.out.println(S() && i == duljina ? "\nDA\n" : "\nNE\n");
	}

	private static boolean S() {
		System.out.printf("S");
		
		if (i != duljina) {
			switch (ulazniNiz.charAt(i++)) {
			case 'a': 
				if (A()) return B();
				else return false;
			case 'b':
				if (B()) return A();
				else return false;
			default:
				return false;
			}
		} else {
			return false;
		}
	}

	private static boolean B() {
		System.out.printf("B");
		if (i != duljina && ulazniNiz.charAt(i) == 'c') {
			i++;
			if (i != duljina && ulazniNiz.charAt(i) == 'c') {
				i++;
				if (i != duljina && S() && i != duljina && ulazniNiz.charAt(i) == 'b') {
					i++;
					if (i != duljina && ulazniNiz.charAt(i) == 'c') {
						i++;
						return true;
					} else return false;
				} else return false;
			} else return false;
		} else return true;
	}

	private static boolean A() {
		System.out.printf("A");
		if (i != duljina) {
			switch (ulazniNiz.charAt(i++)) {
			case 'b':
				return C();
			case 'a':
				return true;
			default:
				return false;
			}
		} else {
			return false;
		}
	}

	private static boolean C() {
		System.out.printf("C");
		if (A()) {
			return A();
		} else {
			return false;
		}
	}
}
