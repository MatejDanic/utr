import java.io.*;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

class Prijelaz1 {
	String trenutnoStanje;
	String simbol;
	String[] iducaStanja;
	
	public Prijelaz1(String trenutnoStanje, String simbol, String[] stanja) {
		this.trenutnoStanje = trenutnoStanje;
		this.simbol = simbol;
		this.iducaStanja = stanja;
	}
	
	public String toString() {
		String rezultat = this.trenutnoStanje + "," + this.simbol + "->";
		for (String stanje : this.iducaStanja) {
			rezultat += stanje + ",";
		}
		return rezultat.substring(0, rezultat.length() - 1);
	}
}

public class SimEnka {
	
	static Prijelaz1[] prijelazi = new Prijelaz1[1500];
	static String pocetnoStanje;
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String[] nizovi, stanja, skupSimbola, prihvStanja, funkcija, ulaz, stanjaPrijelaza;
		String s;
		
//		1. red
		s = reader.readLine();		
		nizovi = s.split("\\|");
		
//		2. red
		s = reader.readLine();
		stanja = s.split("\\,");
		
//		3. red
		s = reader.readLine();	
		skupSimbola = s.split("\\,");
		
//		4. red
		s = reader.readLine();
		prihvStanja = s.split("\\,");
		
//		5. red
		s = reader.readLine();
		pocetnoStanje = s;
		
		int brRed = 5;
		while (true) {
			s = reader.readLine();
			if (s == null) break;
			if (s.isEmpty()) break;
			brRed++;
			funkcija = s.split("->");
			ulaz = funkcija[0].split("\\,");
			stanjaPrijelaza = funkcija[1].split("\\,");
			prijelazi[brRed-6] = new Prijelaz1(ulaz[0], ulaz[1], stanjaPrijelaza);
			
		}
		System.out.println();
		for (String niz : nizovi) {
			System.out.printf(pokreniSimEnka(niz) + "\n");
		}
	}

	@SuppressWarnings("unused")
	private static String pokreniSimEnka(String niz) {
		Set<String> trenutnaStanja = new TreeSet<>();
		Queue<String> iducaStanja = new LinkedList<>();
		Set<String> stanjaZaIspis = new TreeSet<>();
		String stanjaNiza = "";
		String[] znakovi = niz.split("\\,");
		trenutnaStanja.add(pocetnoStanje);
		int indexZnaka = 0;
		boolean postojiPrijelaz = false;
		
		int brojZnakova = 0;
		for (String znak : znakovi) {
			brojZnakova++;
		}
		
		for (String znak : znakovi) {
			
			if ((epsilonOkruzenje(trenutnaStanja, false)).size() == trenutnaStanja.size()) {

				if (trenutnaStanja.size() > 1) trenutnaStanja.remove("#");
				stanjaZaIspis.addAll(trenutnaStanja);
				
				for (String stanje : trenutnaStanja) {
					stanjaNiza += stanje + ",";
				}

				postojiPrijelaz = false;
				for (String stanje : trenutnaStanja) {
					
					int i = 0;
					while (prijelazi[i] != null) {
						if (prijelazi[i].trenutnoStanje.equals(stanje) && prijelazi[i].simbol.equals(znak)) {
							postojiPrijelaz = true;
//							System.out.println(prijelazi[i]);
							for (String novoStanje : prijelazi[i].iducaStanja) {
								iducaStanja.add(novoStanje);
							}
							break;
						}
						i++;
					}
					if (!postojiPrijelaz) {
						iducaStanja.add("#");
					}
				}
				if (iducaStanja.size() > 1) iducaStanja.remove("#");
				stanjaZaIspis.addAll(iducaStanja);
				trenutnaStanja.clear();
				trenutnaStanja.addAll(iducaStanja);
				iducaStanja.clear();
			
			} else {
				if (trenutnaStanja.size() > 1) trenutnaStanja.remove("#");
				stanjaZaIspis.addAll(trenutnaStanja);
				trenutnaStanja = epsilonOkruzenje(trenutnaStanja, true);
			}


			if (trenutnaStanja.size() > 1) trenutnaStanja.remove("#");
			
			if (indexZnaka == brojZnakova-1) {
				stanjaZaIspis.clear();
				trenutnaStanja = epsilonOkruzenje(trenutnaStanja, true);
				if (trenutnaStanja.size() > 1) trenutnaStanja.remove("#");
				stanjaZaIspis.addAll(trenutnaStanja);
				stanjaNiza = stanjaNiza.substring(0, stanjaNiza.length() - 1) + "|";
				for (String stanje : stanjaZaIspis) {
					stanjaNiza += stanje + ",";
				}
				stanjaNiza = stanjaNiza.substring(0, stanjaNiza.length() - 1) + "|";

			}
			stanjaZaIspis.clear();
			stanjaNiza = stanjaNiza.substring(0, stanjaNiza.length() - 1) + "|";
			indexZnaka++;
		}
		return stanjaNiza.substring(0, stanjaNiza.length() - 1);
	}

	private static Set<String> epsilonOkruzenje(Set<String> trenutnaStanja, boolean zadnji) {
		Queue<String> novaStanja = new LinkedList<>();
		boolean imaEpsilon = false;
		int velicinaNova, velicinaStara;
		velicinaStara = 0;
		velicinaNova = trenutnaStanja.size();
		
		while (velicinaNova != velicinaStara) {
			velicinaStara = velicinaNova;
			for (String stanje : trenutnaStanja) {
				imaEpsilon = false;
				int i = 0;
				while (prijelazi[i] != null) {
					if (prijelazi[i].trenutnoStanje.equals(stanje) && prijelazi[i].simbol.equals("$")) {
//						System.out.println(prijelazi[i]);
						for (String novoStanje : prijelazi[i].iducaStanja) {
							novaStanja.add(novoStanje);
						}
						imaEpsilon = true;
						break;
					}
					i++;
				}
				if (!imaEpsilon && zadnji) novaStanja.remove(stanje);
			}
			trenutnaStanja.addAll(novaStanja);
			velicinaNova = trenutnaStanja.size();
		}
		return trenutnaStanja;
	}
}