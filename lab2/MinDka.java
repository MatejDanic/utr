import java.io.*;
import java.util.*;

@SuppressWarnings("rawtypes")
class Par implements Comparable {
	String stanje1, stanje2;
	
	public Par(String stanje1, String stanje2) {
		if (!stanje1.equals(stanje2)) {
			this.stanje1 = stanje1;
			this.stanje2 = stanje2;
		}
	}
	
	public String toString() {
			return "(" + stanje1 + "," + stanje2 + ")";
	}
	
	public int compareTo(Object o) {
		Par noviPar = (Par)o;
		int comp = stanje1.compareTo(noviPar.stanje1);
		return ((comp == 0) ? stanje2.compareTo(noviPar.stanje2) : comp);
	}
	
	public boolean equals(Object o) {
		Par noviPar = (Par)o;
		return stanje1.equals(noviPar.stanje1) && stanje2.equals(noviPar.stanje2) ||
				stanje1.equals(noviPar.stanje2) && stanje2.equals(noviPar.stanje1); 
	}
}


class Prijelaz2 {
	String trenutnoStanje;
	String simbol;
	String iduceStanje;
	
	public Prijelaz2(String trenutnoStanje, String simbol, String iduceStanje) {
		this.trenutnoStanje = trenutnoStanje;
		this.simbol = simbol;
		this.iduceStanje = iduceStanje;
	}
	
	public String toString() {
		return this.trenutnoStanje + "," + this.simbol + "->" + this.iduceStanje;
	}
}

public class MinDka {
	
	static List<Prijelaz2> prijelazi = new ArrayList<>();
	static String pocetnoStanje;
	
	static Set<String> skupStanja = new TreeSet<>();
	static Set<String> skupSimbola = new TreeSet<>();
	static Set<String> skupPrihvStanja = new TreeSet<>();
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String[] stanja, simboli, prihvStanja, funkcija, ulaz;
		String s, iduceStanje;
		
//		1. red
		s = reader.readLine();		
		stanja = s.split("\\,");
		for (String stanje : stanja) {
			skupStanja.add(stanje);
		}
		
//		2. red
		s = reader.readLine();
		simboli = s.split("\\,");
		for (String simbol : simboli) {
			skupSimbola.add(simbol);
		}
		
//		3. red
		s = reader.readLine();	
		prihvStanja = s.split("\\,");
		for (String prihvStanje : prihvStanja) {
			skupPrihvStanja.add(prihvStanje);
		}
		
//		4. red
		s = reader.readLine();
		pocetnoStanje = s;
		
		while (true) {
		
			s = reader.readLine();
			if (s == null) break;
			if (s.isEmpty()) break;
			funkcija = s.split("->");
			ulaz = funkcija[0].split("\\,");
			iduceStanje = funkcija[1];
			prijelazi.add(new Prijelaz2(ulaz[0], ulaz[1], iduceStanje));
			
		}
		izbaciNedohv();
		izbaciIstovjetna();
		
		ispisiIzlaz();
	}


	private static void ispisiIzlaz() {
		
		String izlaz = "";
		for (String stanje : skupStanja) {
			izlaz += stanje + ",";
		}
		System.out.println(izlaz.substring(0, izlaz.length() - 1));
		
		izlaz = "";
		for (String simbol : skupSimbola) {
			izlaz += simbol + ",";
		}
		System.out.println(izlaz.substring(0, izlaz.length() - 1));
		
		izlaz = "";
		for (String stanje : skupPrihvStanja) {
			izlaz += stanje + ",";
		}
		if (skupPrihvStanja.isEmpty()) {
			System.out.println();
		} else { 
			System.out.println(izlaz.substring(0, izlaz.length() - 1));
		}
		
		System.out.println(pocetnoStanje);
		
		izlaz = "";
		for (Prijelaz2 prijelaz : prijelazi) {
			izlaz += prijelaz + "\n";
		}
		System.out.println(izlaz.substring(0, izlaz.length() - 1) + "\n");
	}


	private static void izbaciNedohv() {
		
		Set<String> dohvStanja = new TreeSet<>();
		Queue<String> stanjaZaDodat = new LinkedList<>();
		int trenutniBrojStanja = 0;
		dohvStanja.add(pocetnoStanje);
		stanjaZaDodat.add(pocetnoStanje);
		int noviBrojStanja = 1;
		
		while (trenutniBrojStanja != noviBrojStanja) {
			trenutniBrojStanja = noviBrojStanja;
			for (String stanje : dohvStanja) {
			
				for (String simbol : skupSimbola) {
				
					for (Prijelaz2 prijelaz : prijelazi) {
					
						if (prijelaz.trenutnoStanje.equals(stanje) && prijelaz.simbol.equals(simbol)) {
							stanjaZaDodat.add(prijelaz.iduceStanje);
						}
					}
				}
			}
			
			dohvStanja.addAll(stanjaZaDodat);
			stanjaZaDodat.clear();
			noviBrojStanja = dohvStanja.size();
		}
		
		List<Prijelaz2> prijelaziZaBrisat = new ArrayList<>();
		
		
		
		skupStanja.clear();
		skupStanja.addAll(dohvStanja);
		for (Prijelaz2 prijelaz : prijelazi) {
			if (!skupStanja.contains(prijelaz.trenutnoStanje) ||
				!skupStanja.contains(prijelaz.iduceStanje)) prijelaziZaBrisat.add(prijelaz);
		}
		
		prijelazi.removeAll(prijelaziZaBrisat);
		
		Set<String> zaDodat = new TreeSet<>();
		for (String stanje : skupPrihvStanja) {
			if (skupStanja.contains(stanje)) {
				zaDodat.add(stanje);
			}
		}
		skupPrihvStanja.clear();
		skupPrihvStanja.addAll(zaDodat);
	}

	private static void izbaciIstovjetna() {

		Set<Par> parovi = new TreeSet<>();
		int i = 0, j;
		for (String stanje1 : skupStanja) {
			j = 0;
			for (String stanje2 : skupStanja) {
				if (j > i) {
					parovi.add(new Par(stanje1, stanje2));
				}
				j++;
			}
			i++;
		}
		int trenutniBrojParova = 0;
		int noviBrojParova = parovi.size();
		
		Queue<Par> neIstovjetniParovi = new LinkedList<>();
		
//		for (Par par : parovi) {
//			System.out.println(par);
//		}
//		System.out.println("----------------------------------");
		
//		prvi prolaz
		i = 0;
		for (Prijelaz2 prijelaz1 : prijelazi) {
			j = 0;
			for (Prijelaz2 prijelaz2 : prijelazi) {
				if (j > i && prijelaz1.simbol.equals(prijelaz2.simbol) &&
						(skupPrihvStanja.contains(prijelaz1.iduceStanje) && !skupPrihvStanja.contains(prijelaz2.iduceStanje) ||
						!skupPrihvStanja.contains(prijelaz1.iduceStanje) && skupPrihvStanja.contains(prijelaz2.iduceStanje))) {
					neIstovjetniParovi.add(new Par(prijelaz1.iduceStanje, prijelaz2.iduceStanje));
				}				
				j++;
			}
			i++;
		}
		parovi.removeAll(neIstovjetniParovi);
//		System.out.println("neistovjetni su " + neIstovjetniParovi.toString());
		
		
		while (trenutniBrojParova != noviBrojParova) {
			trenutniBrojParova = noviBrojParova;
			
			for (Par par : parovi) {
				
				for (Prijelaz2 prijelaz1 : prijelazi) {
					
					for (Prijelaz2 prijelaz2 : prijelazi) {
						if (prijelaz1.trenutnoStanje.equals(par.stanje1) && prijelaz2.trenutnoStanje.equals(par.stanje2) && 
							prijelaz1.simbol.equals(prijelaz2.simbol) && !prijelaz1.iduceStanje.equals(prijelaz2.iduceStanje) &&
							neIstovjetniParovi.contains(new Par(prijelaz1.iduceStanje, prijelaz2.iduceStanje))) {
//							System.out.println("promatramo par: " + par);
//							System.out.println(prijelaz1);
//							System.out.println(prijelaz2);
//							System.out.println(prijelaz1.iduceStanje + " i " + prijelaz2.iduceStanje + " nisu istovjetna pa dodajemo i par: " + par + "\n");
							neIstovjetniParovi.add(par);
						}
						
					}
					
				}
			}
			parovi.removeAll(neIstovjetniParovi);
			noviBrojParova = parovi.size();
		}
		
		
		Queue<Prijelaz2> zaBrisat = new LinkedList<>();
		for (Par par : parovi) {
//			System.out.println(par);
			skupStanja.remove(par.stanje2);
			skupPrihvStanja.remove(par.stanje2);
			for (Prijelaz2 prijelaz : prijelazi) {
				if (prijelaz.iduceStanje.equals(par.stanje2)) {
					prijelaz.iduceStanje = par.stanje1;
				}
				if (prijelaz.trenutnoStanje.equals(par.stanje2)) {
//					System.out.println(prijelaz);
					zaBrisat.add(prijelaz);
				}
			}
			if (pocetnoStanje.equals(par.stanje2)) {
				pocetnoStanje = par.stanje1;
			}
			prijelazi.removeAll(zaBrisat);
		}
		
	}
	
}